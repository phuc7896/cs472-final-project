-- MySQL dump 10.13  Distrib 8.0.17, for macos10.14 (x86_64)
--
-- Host: localhost    Database: ProjectsManagement
-- ------------------------------------------------------
-- Server version	8.0.17

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` varchar(500) DEFAULT NULL,
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'mpp-final','This is final project for MPP. Enjoy.','2019-11-20 14:46:12'),(2,'WAP Final Project','Web Application Programming','2019-11-20 14:48:05'),(3,'Demo 1','Project Demo 1','2019-11-21 09:33:36');
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project_task`
--

DROP TABLE IF EXISTS `project_task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `project_task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `projectId` int(11) NOT NULL,
  `taskId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Pproject_id_idx` (`projectId`),
  KEY `Ptask_id_idx` (`taskId`),
  CONSTRAINT `Pproject_id` FOREIGN KEY (`projectId`) REFERENCES `project` (`id`),
  CONSTRAINT `Ptask_id` FOREIGN KEY (`taskId`) REFERENCES `task` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project_task`
--

LOCK TABLES `project_task` WRITE;
/*!40000 ALTER TABLE `project_task` DISABLE KEYS */;
/*!40000 ALTER TABLE `project_task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task`
--

DROP TABLE IF EXISTS `task`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `task` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `description` text NOT NULL,
  `status` tinyint(10) DEFAULT '0',
  `createdDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `projectId` int(11) NOT NULL,
  `startDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `endDate` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task`
--

LOCK TABLES `task` WRITE;
/*!40000 ALTER TABLE `task` DISABLE KEYS */;
INSERT INTO `task` VALUES (7,'Document requirement','Nec sodales dui condimentum tempus, arcu tempus ante nec viverra adipiscing ligula hendrerit? Eros odio adipiscing cras rhoncus faucibus aliquam mauris. Sapien parturient nulla imperdiet orci. Inceptos aptent facilisi pharetra. Luctus lorem per augue ligula adipiscing nibh magnis, elementum senectus id vestibulum. Justo dictum dictumst nostra nibh venenatis interdum ornare cubilia dolor nascetur. Sagittis cum habitant suscipit litora. Metus nisl erat aliquet. Eu rutrum primis conubia ultrices sodales eget.',3,'2019-11-20 14:53:08',1,'2019-11-21 00:00:00','2019-11-23 00:00:00'),(8,'Daily meeting','Setting up a daily meeting. All staff are require. Iaculis senectus ut pharetra convallis metus gravida ac lorem sagittis ligula risus! Curabitur lobortis sit facilisis facilisis amet himenaeos augue. Primis nostra nascetur tortor netus viverra ullamcorper magnis bibendum aliquam condimentum! At rhoncus fames cum, odio laoreet rhoncus. A urna lacinia habitant leo aliquet, aliquam gravida id malesuada. Mollis semper viverra erat, faucibus fusce etiam per habitasse proin vel? Fames dignissim urna eleifend.',2,'2019-11-20 14:57:11',1,'2019-11-20 00:00:00','2019-11-30 00:00:00'),(9,'User module','Implement user module.',2,'2019-11-20 14:57:44',1,'2019-11-20 00:00:00','2019-11-29 00:00:00'),(10,'Implement DAO layer','Implement Data access layer',1,'2019-11-20 14:58:15',1,'2019-11-21 00:00:00','2019-11-23 00:00:00'),(11,'Unit tests','Integrate junit and implement test units.',2,'2019-11-20 14:58:44',1,'2019-11-21 00:00:00','2019-11-30 00:00:00'),(12,'Customer meeting','Iaculis senectus ut pharetra convallis metus gravida ac lorem sagittis ligula risus! Curabitur lobortis sit facilisis facilisis amet himenaeos augue. Primis nostra nascetur tortor netus viverra ullamcorper magnis bibendum aliquam condimentum! At rhoncus fames cum, odio laoreet rhoncus. A urna lacinia habitant leo aliquet, aliquam gravida id malesuada. Mollis semper viverra erat, faucibus fusce etiam per habitasse proin vel? Fames dignissim urna eleifend.',1,'2019-11-20 14:59:22',2,'2019-11-22 00:00:00','2019-11-21 00:00:00'),(13,'Task demo 1','mysql> delete from user_project where project=3;\r\nERROR 1054 (42S22): Unknown column \'project\' in \'where clause\'\r\nmysql> delete from user_project where projectId=3;\r\nQuery OK, 2 rows affected (0.00 sec)',2,'2019-11-21 09:34:09',3,'2019-11-21 00:00:00','2019-11-22 00:00:00');
/*!40000 ALTER TABLE `task` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `task_user`
--

DROP TABLE IF EXISTS `task_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `task_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `taskId` int(11) NOT NULL,
  `userId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Ttask_id_idx` (`taskId`),
  KEY `Tuser_id_idx` (`userId`),
  CONSTRAINT `Ttask_id` FOREIGN KEY (`taskId`) REFERENCES `task` (`id`),
  CONSTRAINT `Tuser_id` FOREIGN KEY (`userId`) REFERENCES `user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `task_user`
--

LOCK TABLES `task_user` WRITE;
/*!40000 ALTER TABLE `task_user` DISABLE KEYS */;
INSERT INTO `task_user` VALUES (1,7,1),(2,8,1),(3,8,2),(4,8,3),(5,9,3),(6,10,3),(7,11,2),(8,11,3),(9,12,5),(10,13,3);
/*!40000 ALTER TABLE `task_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userName` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `role` varchar(45) DEFAULT NULL,
  `firstName` varchar(45) DEFAULT NULL,
  `lastName` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  `dob` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'admin','1234','admin','admin','admin','admin@gmail.com','2019-11-18 17:32:39'),(2,'test','1234','admin','test','test','test@gmail.com','2019-11-18 17:32:39'),(3,'dev','1234','admin','dev','dev','dev@gmail.com','2019-11-18 17:32:39'),(4,'user','1234','admin','user','user','user@gmail.com','2019-11-18 23:52:41'),(5,'locnv','1234','admin','nguyen','loc','admin@gmail.com','2019-11-18 23:52:41'),(6,'locnv1','1234','admin','Loc','Nguyen','','2019-11-07 00:00:00');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_project`
--

DROP TABLE IF EXISTS `user_project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user_project` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `userId` int(11) NOT NULL,
  `projectId` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `Puser_id_idx` (`userId`),
  KEY `Uproject_id_idx` (`projectId`),
  CONSTRAINT `Puser_id` FOREIGN KEY (`userId`) REFERENCES `user` (`id`),
  CONSTRAINT `Uproject_id` FOREIGN KEY (`projectId`) REFERENCES `project` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_project`
--

LOCK TABLES `user_project` WRITE;
/*!40000 ALTER TABLE `user_project` DISABLE KEYS */;
INSERT INTO `user_project` VALUES (1,1,1),(2,2,1),(3,3,1),(4,2,2),(5,3,2),(6,4,2),(7,3,3);
/*!40000 ALTER TABLE `user_project` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-11-21 11:38:52
