package edu.mum.cs.cs472.task_mgnt.custom_tag;

import java.io.IOException;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import edu.mum.cs.cs472.task_mgnt.model.Task;
import edu.mum.cs.cs472.task_mgnt.type.TaskStatus;

public class TaskStatusCustomTag extends SimpleTagSupport  {
	
	private Task task;
	
	public void setTask(Task task) {
		this.task = task;
	}
	
	public Task getTask() {
		return task;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		super.doTag();
		
       JspWriter out = getJspContext().getOut();
       
       String status = "unknown";
       String clazz = "task-status";
       
       TaskStatus taskStatus = TaskStatus.getTaskStatus(task.getStatus());
       
       switch(taskStatus) {
       case New:
    	   status = "New";
    	   clazz += " task-new";
    	   break;
       case InProcess:
    	   status = "Inprocess";
    	   clazz += " task-inprocess";
    	   break;
       case Finished: 
    	   status = "Finished";
    	   clazz += " task-finish";
    	   break;
       default:
    	   status = "unknown";
    	   clazz = "task-unknown";
    	   break;
    	   
       }
       
       out.println(String.format("Status: <span class='%s'>%s</span>", clazz, status));

	}
	
}