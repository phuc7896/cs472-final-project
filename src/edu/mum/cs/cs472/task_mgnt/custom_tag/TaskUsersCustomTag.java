package edu.mum.cs.cs472.task_mgnt.custom_tag;

import java.io.IOException;
import java.util.List;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.JspWriter;
import javax.servlet.jsp.tagext.SimpleTagSupport;

import edu.mum.cs.cs472.task_mgnt.model.User;

public class TaskUsersCustomTag extends SimpleTagSupport  {
	
	private List<User> users;
	
	public void setUsers(List<User> users) {
		this.users = users;
	}
	
	public List<User> getUsers() {
		return users;
	}
	
	@Override
	public void doTag() throws JspException, IOException {
		super.doTag();
		
       JspWriter out = getJspContext().getOut();
       StringBuilder sb = new StringBuilder();
       
       sb.append("<div class='icon-ls-user'>");
       
       if(users == null || users.isEmpty()) {
    	   sb.append("---");
       } else {
    	   sb.append("Users: ");
    	   users.forEach(u -> {
        	   sb.append("<span class='icon-user'>")
        	   .append(u.getUsername())
        	   .append("</span>");
           });   
       }
       
       sb.append("</div>");
       
       out.println(sb.toString());

	}
	
}