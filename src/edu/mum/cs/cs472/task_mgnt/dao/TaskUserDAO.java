package edu.mum.cs.cs472.task_mgnt.dao;


import java.sql.SQLException;
import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.TaskUser;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

public class TaskUserDAO extends AbstractDAO<TaskUser, Long> {

	private Logger logger;
	
	public TaskUserDAO() {
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		logger.debug("TaskUserDAO initialized.");
	}
	
	public List<TaskUser> findByTask(long taskId) {
		
		try {
			return findByColumn("taskId", taskId);
		} catch (SQLException e) {
			logger.error("Failed to get TaskUser by taskId", e);
			return null;
		}

	}

	/**
	 * Get all tasks belong to a specific user in a specific project
	 * @param projectId
	 * @return
	 */
	//public List<TaskUser> findByProject(Long projectId, Long userId) {
		
		// String query = "select id as userId from ";
		
	//	return null;
	//}
}
