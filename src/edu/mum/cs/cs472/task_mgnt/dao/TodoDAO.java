package edu.mum.cs.cs472.task_mgnt.dao;

import java.sql.SQLException;
import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.Todo;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

public class TodoDAO extends AbstractDAO<Todo, Long> {
	
	private Logger logger;
	
	public TodoDAO() {
		super();
		
		logger = Logger.getLogger(this.getClass().getCanonicalName());
	}
	
	public List<Todo> findTodosByTitle(String title) {
		try {
			return findByColumn("title", title);
		} catch (SQLException e) {
			logger.error("An error occurs while findind todos by title.", e);
			return null;
		}
	}
}
