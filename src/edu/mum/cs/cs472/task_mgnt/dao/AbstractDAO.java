package edu.mum.cs.cs472.task_mgnt.dao;

import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;

import edu.mum.cs.cs472.task_mgnt.model.BaseModel;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

/**
 * Abstract JDBC DAO implementation
 * 
 * SUpporting ID of LONG ONLY. 
 * 
 * @author locnv
 *
 * @param <T>
 * @param <ID>
 */
public abstract class AbstractDAO<T extends BaseModel<ID>, ID> {
	
	private final String DbCtxName = "jdbc/ProjectsManagement";
	private final String DbName = "ProjectsManagement";
	
	private final static String FindAllQueryTemplate = "Select * from `%s`.%s";
	private final static String GetMaxIDQueryTemplate = "Select max(%s) from %s";
	private final static String FindByIdQueryTemplate = "Select * from %s where %s = %s";
	// Select * from user where id in (1, 2, 3)
	private final static String FindByIdsQueryTemplate = "Select * from %s where %s in (%s)";
	private final static String FindByColumnQueryTemplate = "Select * from %s where %s like '%s'";
	// INSERT INTO table_name (column1, column2, ...) VALUES (value1, value2, ...);
	private final static String InsertQueryTemplate = "Insert into %s(%s) values(%s)";
	// Delete from table_name where idColumnName = id_value; (id of long only)
	private final static String DeleteQueryTemplate = "Delete from %s where %s = %d";
	private final static String MultipleDeleteQueryTemplate = "Delete from %s where %s in (%s)";
	// Update table_name set columns where condition
	private final static String UpdateQueryTemplate = "Update %s Set %s where %s";

	
	protected Logger logger;
	
	// @Resource(name = "jdbc/cs472-201911-lesson15-contacts-db")
	protected DataSource dataSource;
	
	public AbstractDAO() {
		
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		
		try {
            Context initContext = new InitialContext();
            Context envContext = (Context) initContext.lookup("java:comp/env");
            this.dataSource = (DataSource) envContext.lookup(DbCtxName);
        } catch (NamingException e) {
            logger.error("Failed to initialize dataSource for " + DbCtxName, e);
        }
	}
	
	protected T getInstanceOfT() {
		ParameterizedType superClass = (ParameterizedType) getClass().getGenericSuperclass();
        @SuppressWarnings("unchecked")
		Class<T> type = (Class<T>) superClass.getActualTypeArguments()[0];
        try {
            return type.newInstance();
        }
        catch (Exception e) {
            throw new RuntimeException(e);
        }
	}
	
	public Long getMaxId(Connection conn) throws Exception {
		
		boolean closeConn = (conn == null);
		
		T t = getInstanceOfT();
        
		String query = String.format(GetMaxIDQueryTemplate, t.getIdColumnName(), t.getTableName());
		logger.debug("MaxIdQuery -> " + query);
		try {
			if(conn == null) {
				conn = dataSource.getConnection();
			}

            PreparedStatement pstmt = conn.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            
            if(rs.next()) {
            	return rs.getLong(1);
            }

		} catch(SQLException e) {
			logger.error("An error occured while executing getMaxId.", e);
			throw e;
		} finally { 
			if(closeConn) tryCloseConnection(conn); 
		}
	
		throw new Exception("Cannot get MaxID");
	}
	
	public T findById(ID id) throws SQLException {
		
		T t = getInstanceOfT();
        String[] columnNames = t.getColumnNames();
        
		String query = String.format(FindByIdQueryTemplate, t.getTableName(), t.getIdColumnName(), id.toString());
		
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            
            if(rs.next()) {
            	T entity = getInstanceOfT();
                Object[] data = new Object[columnNames.length];
            	for(int i = 0; i < columnNames.length; i++) {
            		String key = columnNames[i];
            		Object d = rs.getObject(key);
            		data[i] = d;
            	}
            	
            	entity.loadData(data);
            	
            	return entity;
            }

		} catch(SQLException e) {
			logger.error("An error occured while executing findById.", e);
			throw e;
		} finally { tryCloseConnection(connection); }
		
		return null;
	}
	
	public List<T> findByIds(List<ID> ids) throws SQLException {
		
		if(ids == null || ids.isEmpty()) {
			return null;
		}
		
		//if(ids.size() == 1) {
		//	return findById(ids.get(0));
		//}
		
		T t = getInstanceOfT();
        String[] columnNames = t.getColumnNames();
        StringBuilder sb = new StringBuilder(ids.get(0).toString());
        for(int i = 1; i < ids.size(); i++) {
        	sb.append(",").append(ids.get(i).toString());
        }
        
		String query = String.format(FindByIdsQueryTemplate, t.getTableName(), t.getIdColumnName(), sb.toString());
		
		Connection connection = null;
		List<T> entities = new ArrayList<T>();
		try {
			connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            
            while(rs.next()) {
            	T entity = getInstanceOfT();
                Object[] data = new Object[columnNames.length];
            	for(int i = 0; i < columnNames.length; i++) {
            		String key = columnNames[i];
            		Object d = rs.getObject(key);
            		data[i] = d;
            	}
            	
            	entity.loadData(data);
            	
            	entities.add(entity);
            }

		} catch(SQLException e) {
			logger.error("An error occured while executing findByIds.", e);
			throw e;
		} finally { tryCloseConnection(connection); }
		
		return entities;
	}
	
	/**
	 * Find all entities of T type
	 * 
	 * @return List of T
	 * @throws SQLException
	 */
	public List<T> findAll() throws SQLException {
		
		T t = getInstanceOfT();
        String[] columnNames = t.getColumnNames();
        
		String query = String.format(FindAllQueryTemplate, DbName, t.getTableName());
		
		List<T> entities = new ArrayList<T>();
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            
            while(rs.next()) {
            	T entity = getInstanceOfT();
                Object[] data = new Object[columnNames.length];
            	for(int i = 0; i < columnNames.length; i++) {
            		String key = columnNames[i];
            		Object d = rs.getObject(key);
            		data[i] = d;
            	}
            	
            	entity.loadData(data);
            	
            	entities.add(entity);
            }

		} catch(SQLException e) {
			logger.error("An error occured while executing findAll.", e);
			throw e;
		} finally { tryCloseConnection(connection); }
		
		return entities;
	}
	
	public List<T> findByColumn(String columnName, Object value) throws SQLException {
		T t = getInstanceOfT();
        String[] columnNames = t.getColumnNames();
        
		String query = String.format(FindByColumnQueryTemplate, t.getTableName(), columnName, "%"+value.toString()+"%");
		
		List<T> entities = new ArrayList<T>();
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            ResultSet rs = pstmt.executeQuery();
            
            while(rs.next()) {
            	T entity = getInstanceOfT();
                Object[] data = new Object[columnNames.length];
            	for(int i = 0; i < columnNames.length; i++) {
            		String key = columnNames[i];
            		Object d = rs.getObject(key);
            		data[i] = d;
            	}
            	
            	entity.loadData(data);
            	
            	entities.add(entity);
            }
            
            return entities;

		} catch(SQLException e) {
			logger.error("An error occured while executing findById.", e);
			throw e;
		} finally { tryCloseConnection(connection); }
		
	}
	
	/**
	 * Insert an entity into table
	 * 
	 * @param entity
	 * @return
	 */
	public T add(T entity) {
		T t = getInstanceOfT();
        String[] columnNames = t.getColumnNames();
        
        StringBuilder sb = new StringBuilder(columnNames[0]);
        for(int i = 1; i < columnNames.length; i++) {
        	sb.append(",").append(columnNames[i]);
        }
        
		String columns = sb.toString();
		
		sb.delete(0, sb.length());
		Object[] values = entity.getData();
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			long maxId = getMaxId(connection);
			
			sb.append(maxId + 1);
			for(int i = 1; i < columnNames.length; i++) {
				sb.append(",").append('?');
	        }
			String valuesTxt = sb.toString();
			
			String query = String.format(InsertQueryTemplate, t.getTableName(), columns, valuesTxt);
			logger.debug("Add Entity. Query -> " + query);
			// -> Expecting query string:
			// Insert into contacts(contacts_id,customer_name,gender,category,message) values(7,?,?,?,?)
			
			PreparedStatement pstmt = connection.prepareStatement(query);

			for(int i = 1; i < columnNames.length; i++) {
				Object d = values[i];
				if(d instanceof String) {
					pstmt.setString(i, (String)d);
				} else if(d instanceof Integer) {
					pstmt.setInt(i, (int)d);
				} else if(d instanceof Long) {
					pstmt.setLong(i, (long)d);
				} else if (d instanceof LocalDateTime) {
					Timestamp ts = Timestamp.valueOf((LocalDateTime)d);
					pstmt.setTimestamp(i, ts);
				} else {
					// TODO implement
					// Missing parsing some kind of data....
					logger.error("[BIG-O] Missing value (not a string) -> " + d.toString());
				}
			}
			
            int rs = pstmt.executeUpdate();
            logger.debug("Add Entity. Result -> " + rs);
            
            entity.setId(maxId + 1);
            return entity;
		} catch (Exception e) {
			logger.error("An error occurs while executing add entity.", e);
			return null;
		} finally { tryCloseConnection(connection); }

	}
	
	public void add(T[] entities) {
		throw new UnsupportedOperationException("Function is not implemented yet!!!");
	}
	
	public int update(T entity) {
        String[] columnNames = entity.getColumnNames();
        
        StringBuilder sb = new StringBuilder();
        for(int i = 1; i < columnNames.length; i++) {

        	sb.append(columnNames[i]).append("=?");
        	
        	if(i < columnNames.length -1) {
        		sb.append(",");
        	}
        }
        
		String columns = sb.toString();
		
		String condition = String.format("%s=%d", entity.getIdColumnName(), entity.getId());
		
		// sb.delete(0, sb.length());
		Object[] values = entity.getData();
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
			
			String query = String.format(UpdateQueryTemplate, entity.getTableName(), columns, condition);
			logger.debug("Update Entity. Query -> " + query);
			// -> Expecting query string:
			// Update todo(title,description,creastedAt,status) values(?,?,?,?)
			
			PreparedStatement pstmt = connection.prepareStatement(query);

			for(int i = 1; i < columnNames.length; i++) {
				Object d = values[i];
				if(d instanceof String) {
					pstmt.setString(i, (String)d);
				} else if (d instanceof LocalDateTime) {
					Timestamp ts = Timestamp.valueOf((LocalDateTime)d);
					pstmt.setTimestamp(i, ts);
				} else if(d instanceof Integer) {
					pstmt.setInt(i, (int)d);
				} else if(d instanceof Long) {
					pstmt.setLong(i, (long)d);
				} else {
					// TODO implement
					// Missing parsing some kind of data....
					logger.error("[BIG-O] Missing value (not a string) -> " + d.toString());
				}
			}
			
            int rs = pstmt.executeUpdate();
            logger.debug("Updated Entity. Result -> " + rs);
            return rs;
		} catch (Exception e) {
			logger.error("An error occurs while executing add entity.", e);
			return 0;
		} finally { tryCloseConnection(connection); }
		
	}
	
	public void delete(ID id) {
		T t = getInstanceOfT();
        
		String query = String.format(DeleteQueryTemplate, t.getTableName(), t.getIdColumnName(), id);
		logger.debug("Abstract DAO delete by id. Query -> " + query);
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            int rs = pstmt.executeUpdate();
            logger.debug("Abstract DAO delete by id. Result -> " + rs);
		} catch(SQLException e) {
			logger.error("An error occured while executing delete by id.", e);
		} finally { tryCloseConnection(connection); }
		
	}
	
	public void delete(ID[] ids) {
		if(ids == null) {
			return;
		}
		if(ids.length == 1) {
			delete(ids[0]);
			return;
		}
		
		T t = getInstanceOfT();
        StringBuilder sb = new StringBuilder(ids[0].toString());
        for(int i = 1; i < ids.length; i++) {
        	sb.append(',');
        	sb.append(ids[i].toString());
        }
		String id = sb.toString();
		String query = String.format(MultipleDeleteQueryTemplate, t.getTableName(), t.getIdColumnName(), id);
		logger.debug("Abstract DAO delete by ids. Query -> " + query);
		Connection connection = null;
		try {
			connection = dataSource.getConnection();
            PreparedStatement pstmt = connection.prepareStatement(query);
            int rs = pstmt.executeUpdate();
            logger.debug("Abstract DAO delete by ids. Result -> " + rs);
		} catch(SQLException e) {
			logger.error("An error occured while executing delete by ids.", e);
		} finally { tryCloseConnection(connection); }
		
	}
	
	protected void tryCloseConnection(Connection conn) {
		if(conn != null) {
			try {
				conn.close();
			} catch (Exception e) { }
		}
	}
}
