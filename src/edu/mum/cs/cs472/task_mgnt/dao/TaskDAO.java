package edu.mum.cs.cs472.task_mgnt.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.Task;

public class TaskDAO extends AbstractDAO<Task, Long> {
	public List<Task> findByUserAndProject(Long userId, Long projectId) throws SQLException {
		Connection connection = null;
		String query = "select * from task inner join user_project on task.projectId = user_project.projectId where user_project.userId = " + userId + " and user_project.projectId = " + projectId;
		try {
			connection = dataSource.getConnection();
			PreparedStatement pstmt = connection.prepareStatement(query);
	        ResultSet rs = pstmt.executeQuery();
	        List<Task> listTasks = new ArrayList<Task>();
	        while(rs.next()) {
	        	Task t = new Task(rs.getString("name"), rs.getString("description"), rs.getLong("projectId"), rs.getInt("status"));
	        	listTasks.add(t);
	        }
	        return listTasks;
		} catch(SQLException e) {
			System.out.println(e);
			return null;
		} finally {
			tryCloseConnection(connection);
		}
	}
}
