package edu.mum.cs.cs472.task_mgnt.type;

public enum TaskStatus {
	Unknown(0),
	New(1),
	InProcess(2),
	Finished(3);
	
	private int value;
	
	TaskStatus(int value) {
		this.value = value;
	}
	
	public int getValue() {
		return value;
	}
	
	public void setValue(int value) {
		this.value = value;
	}
	
	public static TaskStatus getTaskStatus(int v) {
		
      for(TaskStatus ts : TaskStatus.values()) {
          if (ts.getValue() == v) {
        	  return ts;
          }
      }
      
      return TaskStatus.Unknown;
		
	}
}
