package edu.mum.cs.cs472.task_mgnt.type;

public enum UserRole {
	Admin, PM, Dev
}
