package edu.mum.cs.cs472.task_mgnt.api;

import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.google.gson.Gson;

import edu.mum.cs.cs472.task_mgnt.controller.BaseServlet;
import edu.mum.cs.cs472.task_mgnt.model.Task;
import edu.mum.cs.cs472.task_mgnt.service.IProjectService;
import edu.mum.cs.cs472.task_mgnt.service.ITaskService;
import edu.mum.cs.cs472.task_mgnt.service.impl.ServiceFactory;
import edu.mum.cs.cs472.task_mgnt.type.TaskStatus;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

/**
 * Servlet implementation class TaskMgntApi
 */
@WebServlet("/api/v1")
public class TaskMgntApi extends BaseServlet {
	private static final long serialVersionUID = 1L;
	
	private static final String CMD_CHECK_PROJ_NAME_EXIST = "chk_proj_name_exist";
	private static final String CMD_UPDATE_TASK_STATUS = "update-task-status";
	
	private IProjectService projectService;
	private ITaskService taskService;
	private Gson gson;
	private Logger logger;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TaskMgntApi() {
        super();

        projectService = ServiceFactory.getInstance().getProjectService();
        taskService = ServiceFactory.getInstance().getTaskService();
        
        logger = Logger.getLogger(this.getClass().getCanonicalName());
        logger.info("Task Managemnt API V1 initialized.");
        
        gson = new Gson();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
//	protected void doGet(
//		HttpServletRequest request, 
//		HttpServletResponse response) 
//		throws ServletException, IOException {
//		
//		response.getWriter()
//		.append("Served at: ")
//		.append(request.getContextPath());
//	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) 
		throws ServletException, IOException {
		
		response.setContentType("application/json");
		response.setCharacterEncoding("UTF-8");
		response.setStatus(HttpServletResponse.SC_OK);
		
		String command = request.getParameter("cmd");
		String jsonStr = "";
		switch (command) {
		case CMD_CHECK_PROJ_NAME_EXIST:
			String projectName = request.getParameter("projectName");
			jsonStr = checkProjectNameExist(projectName);
			break;
		case CMD_UPDATE_TASK_STATUS:
			try {
				String reqBody = getBody(request);
				Task task = gson.fromJson(reqBody, Task.class);
				logger.debug("Updating task -> " + task.toString());
				jsonStr = updateTaskStatus(task.getId(), TaskStatus.getTaskStatus(task.getStatus()));	
			} catch (Exception e) {
				jsonStr = buildErrorMessage();
			}
			
			break;
		default:
			jsonStr = "{ serverTime: " + LocalDateTime.now() + " }";
			break;
			
		}
		
		PrintWriter out = response.getWriter();
		out.println(jsonStr);
	}
	
	private String updateTaskStatus(long taskId, TaskStatus status) {
		boolean isOk = false;
		Task task = taskService.getTaskById(taskId);
		if(task != null) {
			task.setStatus(status.getValue());
			taskService.updateTask(task);
			isOk = true;
		}
		
		return "{"
				+ "\"rs\": "+ isOk +","
				+ "\"serverTime\": \"" + LocalDateTime.now()
			 + "\" }";
	}
	
	private String checkProjectNameExist(String projectName) {
		
		boolean rs = projectService.checkProjectNames(projectName);
		
		return "{"
				+ "rs: "+ rs +","
				+ "serverTime: " + LocalDateTime.now()
			 + "}";
	}
	
	private String buildErrorMessage() {
		return "{"
				+ "rs: false,"
				+ "err: 'Server error',"
				+ "serverTime: " + LocalDateTime.now()
			 + "}";
	}

}
