package edu.mum.cs.cs472.task_mgnt.service;

import java.util.List;

import edu.mum.cs.cs472.task_mgnt.model.Todo;

public interface ITodoService {
	Todo getTodoById(Long todoId);
	List<Todo> getAllTodos();
	Todo addTodo(Todo todo);
	Todo updateTodo(Todo todo);
	Todo deleteTodo(Long todoId);
}
