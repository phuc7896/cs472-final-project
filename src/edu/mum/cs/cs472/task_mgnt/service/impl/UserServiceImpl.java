package edu.mum.cs.cs472.task_mgnt.service.impl;

import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import edu.mum.cs.cs472.task_mgnt.dao.TaskUserDAO;
import edu.mum.cs.cs472.task_mgnt.dao.UserDAO;
import edu.mum.cs.cs472.task_mgnt.model.TaskUser;
import edu.mum.cs.cs472.task_mgnt.model.User;
import edu.mum.cs.cs472.task_mgnt.service.IUserService;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

public class UserServiceImpl implements IUserService {
	
	private UserDAO userDao;
	private TaskUserDAO taskUserDao;
	private Logger logger;
		
	public UserServiceImpl() {
		userDao = new UserDAO();
		taskUserDao = new TaskUserDAO();
		
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		
		logger.debug("UserService initialized!");
		
		installDefaultUsers();
	}
	
	private void installDefaultUsers() {
		List<User> allUsers = new ArrayList<User>();
		
		User admin = new User("admin", "1234", "admin", "admin", "admin@gmail.com", LocalDateTime.now());
		admin.setId(1);
		User user = new User("user", "1234", "user", "user", "user@gmail.com", LocalDateTime.now());
		user.setId(2);
		User locnv = new User("locnv", "1234", "nguyen", "loc", "admin@gmail.com", LocalDateTime.now());
		locnv.setId(3);
		
		allUsers.addAll(Arrays.asList(new User[] {
				admin, user, locnv
		}));
		
		allUsers.forEach(u -> {
			try {
				User dbUser = userDao.findByUserName(u.getUsername());
				
				if(dbUser == null) {
					User addedUser = userDao.add(u);
					logger.debug("Added new user -> " + addedUser.getUsername());
				}
			} catch(Exception ex) {
				
			}
		});
			
		
	}

	@Override
	public boolean login(User user) {
		// TODO Auto-generated method stub
		return true;
	}

	@Override
	public User addUser(User user) throws Exception {
		
		User dbUser = userDao.findByUserName(user.getUsername());
		if(dbUser != null) {
			throw new Exception("User existed.");
		}
		
		return userDao.add(user);
	}

	@Override
	public User updateUser(User user) throws Exception {

		User dbUser = userDao.findByUserName(user.getUsername());
		if(dbUser == null) {
			throw new Exception("Cannot update an non-exist user.");
		}
		user.setId(dbUser.getId());
		userDao.update(user);
		
		return user;
	}

	@Override
	public User getUserById(Long userId) throws Exception {
		return userDao.findById(userId);
	}

	@Override
	public List<User> getAllUsers() {
		try {
			return userDao.findAll();
		} catch(Exception e) {
			logger.error("An error occurs while getting all Users.", e);
			return null;
		}
	}

	@Override
	public void deleteUser(Long userId) {
		userDao.delete(userId);
	}
	
	public List<User> getUsersByProjectId(Long projectId) throws Exception {
		List<Long> listUserId = userDao.findUserIdByProjectId(projectId);
		List<User> users = new ArrayList<User>();
		listUserId.forEach(userId -> {
			try {
				users.add(userDao.findById(userId));
			} catch (SQLException e) {
				e.printStackTrace();
			}
		});
		return users;
	}

	@Override
	public List<User> getUsersByTaskId(Long taskId) {
		List<TaskUser> taskProject = taskUserDao.findByTask(taskId);
		if(taskProject == null || taskProject.isEmpty()) {
			return null;
		}
		
		List<Long> userIds = taskProject.stream()
		.map(TaskUser::getUserId)
		.collect(Collectors.toList());
		
		try {
			return userDao.findByIds(userIds);
		} catch (Exception e) {
			logger.error("Failed to exeucte findByIds", e);
			return null;
		}
	}

	@Override
	public User login(String username, String password) {
		try {
			User user = userDao.findByUserName(username);
			if (user == null || (user != null && !user.getPassword().equals(password))) {
				return null;
			}
			return user;
		} catch (Exception e) {
			logger.error("Failed to login", e);
			return null;
		}
	}
}
