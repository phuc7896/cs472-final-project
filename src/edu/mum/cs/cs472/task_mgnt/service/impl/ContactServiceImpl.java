package edu.mum.cs.cs472.task_mgnt.service.impl;

import java.util.List;

import edu.mum.cs.cs472.task_mgnt.dao.ContactDAO;
import edu.mum.cs.cs472.task_mgnt.model.Contact;
import edu.mum.cs.cs472.task_mgnt.service.IContactService;
import edu.mum.cs.cs472.task_mgnt.util.Logger;

public class ContactServiceImpl implements IContactService {
	
	private ContactDAO contactDao;
	private Logger logger;
		
	public ContactServiceImpl() {
		contactDao = new ContactDAO();
		
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		
		logger.debug("UserService initialized!");

	}
	
	@Override
	public Contact addContact(Contact contact) {
		
		return contactDao.add(contact);
	}

	@Override
	public List<Contact> getAllContacts() {
		try {
			return contactDao.findAll();
		} catch(Exception e) {
			logger.error("An error occurs while getting all contacts.", e);
			return null;
		}
	}

}
