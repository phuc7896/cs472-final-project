package edu.mum.cs.cs472.task_mgnt.service.impl;

import java.util.HashMap;
import java.util.Map;

import edu.mum.cs.cs472.task_mgnt.service.IContactService;
import edu.mum.cs.cs472.task_mgnt.service.IProjectService;
import edu.mum.cs.cs472.task_mgnt.service.ITaskService;
import edu.mum.cs.cs472.task_mgnt.service.IUserService;

public class ServiceFactory {
	
	private static ServiceFactory instance = null;
	
	 private static Map<String, Object> serviceMap;
	
	private ServiceFactory() {
		 serviceMap = new HashMap<String, Object>();
	}
	
	public static ServiceFactory getInstance() {
		if(instance == null) {
			instance = new ServiceFactory();
		}
		
		return instance;
	}
	
	public IProjectService getProjectService() {
		String key = IProjectService.class.getCanonicalName();
		Object obj = serviceMap.get(key);
		
		if(obj == null) {
			obj = new ProjectServiceImpl();
			serviceMap.put(key, obj);
		}
		
		return (IProjectService) obj;
	}
	
	public IUserService getUserService() {
		String key = IUserService.class.getCanonicalName();
		Object obj = serviceMap.get(key);
		
		if(obj == null) {
			obj = new UserServiceImpl();
			serviceMap.put(key, obj);
		}
		
		return (IUserService) obj;
	}
	
	public ITaskService getTaskService() {
		String key = ITaskService.class.getCanonicalName();
		Object obj = serviceMap.get(key);
		
		if(obj == null) {
			obj = new TaskServiceImpl();
			serviceMap.put(key, obj);
		}
		
		return (ITaskService) obj;
	}
	
	public IContactService getContactService() {
		String key = IContactService.class.getCanonicalName();
		Object obj = serviceMap.get(key);
		
		if(obj == null) {
			obj = new ContactServiceImpl();
			serviceMap.put(key, obj);
		}
		
		return (IContactService) obj;
	}
//	
//	public Object getService(Class<?> serviceClazz) {
//		
//		String key = serviceClazz.getCanonicalName();
//		Object service = serviceMap.get(key);
//		if(service == null) {
//			service = loadService(serviceClazz);
//			serviceMap.put(key, service);
//		}
//		
//		return service;
//	}
//	
//	private Object loadService(Class<?> serviceClazz) {
//		
//		ParameterizedType superClass = (ParameterizedType) serviceClazz.getGenericSuperclass();
//
//		Class<?> type = (Class<?>) superClass.getActualTypeArguments()[0];
//        try {
//            return type.newInstance();
//        }
//        catch (Exception e) {
//            throw new RuntimeException(e);
//        }
//
//	}
}
