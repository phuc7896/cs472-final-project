package edu.mum.cs.cs472.task_mgnt.service.impl;

import java.sql.SQLException;
import java.util.List;

import edu.mum.cs.cs472.task_mgnt.dao.TaskDAO;
import edu.mum.cs.cs472.task_mgnt.dao.TaskUserDAO;
import edu.mum.cs.cs472.task_mgnt.model.Task;
import edu.mum.cs.cs472.task_mgnt.service.ITaskService;
import edu.mum.cs.cs472.task_mgnt.util.Logger;
import edu.mum.cs.cs472.task_mgnt.model.TaskUser;

public class TaskServiceImpl implements ITaskService {
	
	private TaskDAO taskDao;
	private TaskUserDAO taskUserDao;
	private Logger logger;
	
	public TaskServiceImpl() {
		taskDao = new TaskDAO();
		taskUserDao = new TaskUserDAO();
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		
		logger.debug("TaskService initialized.");
	}

	@Override
	public Task getTaskById(Long taskId) {
		try {
			return taskDao.findById(taskId);	
		} catch(Exception ex) {
			logger.error("Error: ", ex);
			return null;
		}
		
	}
	
	@Override
	public List<Task> getTasksByProject(Long projectId) {
		try {
			return taskDao.findByColumn("projectId", projectId);	
		} catch(Exception ex) {
			logger.error("Error: ", ex);
			return null;
		}

	}
	
	public List<Task> getTasksByUserAndProject(Long userId, Long projectId) {	
		try {
			return taskDao.findByUserAndProject(userId, projectId);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}		
	}

	@Override
	public List<Task> getAllTasks() {
		try {
			return taskDao.findAll();
		} catch(Exception ex) {
			logger.error("Error: ", ex);
			return null;
		}
	}

	@Override
	public Task addTask(Task task, List<Long> users) {
		try {
			Task t = taskDao.add(task);
			users.forEach(user -> {
				taskUserDao.add(new TaskUser(t.getId(), user));
			});
			return t;
		} catch(Exception ex) {
			logger.error("Error: ", ex);
			return null;
		}
	}

	@Override
	public Task updateTask(Task task) {
		try {
			taskDao.update(task);
			return task;	
		} catch(Exception ex) {
			logger.error("Error: ", ex);
			return null;
		}
		
	}

	@Override
	public Task deleteTask(Long taskId) {

		try {
			Task dbTask = getTaskById(taskId);
			if(dbTask == null) {
				return null;
			}
			
			taskDao.delete(taskId);
			
			return dbTask;	
		} catch(Exception ex) {
			logger.error("Error: ", ex);
			return null;
		}
		
	}

}
