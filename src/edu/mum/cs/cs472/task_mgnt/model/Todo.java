package edu.mum.cs.cs472.task_mgnt.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Todo extends BaseModel<Long> {
	
	private final String TableName = "todo";
	private final String[] ColumnNames = { 
	"id", "title", "description", "status", "createdAt" };
	
	private long id;
	private String title;
	private String description;
	// 0 / 1 / 2
	private int status;
	private LocalDateTime createdAt;
	
	public Todo() {
		
	}
	
	public Todo(String title, String description) {
		
		super();
		
		this.title = title;
		this.description = description;
		this.status = 1;
		this.createdAt = LocalDateTime.now();
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}
	
	public void setTitle(String title) {
		this.title = title;
	}
	
	public String getDescription() {
		return description;
	}
	
	public void setDescription(String description) {
		this.description = description;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public int getStatus() {
		return status;
	}
	
	public LocalDateTime getCreatedAt() {
		return createdAt;
	}
	
	public void setCreatedAt(LocalDateTime createdAt) {
		this.createdAt = createdAt;
	}
	
	@Override
	public String toString() {
		String createdTxt = createdAt == null ? "" : createdAt.toString();
		return String.format("Todo {"
				+ "Title: %s,"
				+ "Description: %s,"
				+ "CreatedAt: %s,"
				+ "Status: %d"
				+ "}", 
				title, description, createdTxt, status);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public void loadData(Object[] data) {
		id = new Long((int)data[0]);
		title = data[1].toString();
		description = data[2].toString();
		status = (int)data[3];
		Timestamp sqlDate = (Timestamp)data[4];
		LocalDateTime dt = sqlDate.toLocalDateTime();
		createdAt = dt;
	}
	
	@Override
	public Object[] getData() {
		return new Object[] {
			id, title, description, status, createdAt
		};
	}
	
}
