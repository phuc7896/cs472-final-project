package edu.mum.cs.cs472.task_mgnt.model;

public class UserProject extends BaseModel<Long> {
	
	private final String TableName = "user_project";
	private final String[] ColumnNames = { "id", "userId", "projectId" };
	
	private long id;
	private long projectId;
	private long userId;
	
	public UserProject() { }

	public UserProject(long projectId, long userId) {
		super();
		this.projectId = projectId;
		this.userId = userId;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public long getUserId() {
		return userId;
	}
	
	public void setUserId(long userId) {
		this.userId = userId;
	}
	
	public long getProjectId() {
		return projectId;
	}
	
	public void setProjectId(long projectId) {
		this.projectId = projectId;
	}

	@Override
	public String toString() {
		
		return String.format("UserProject: {"
				+ "id: %d,"
				+ "userId: %d,"
				+ "projectId: %d}", 
				id, userId, projectId);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public void loadData(Object[] data) {
		id = Long.parseLong(data[0].toString());
		userId = Long.parseLong(data[1].toString());
		projectId = Long.parseLong(data[2].toString());
	}
	
	@Override
	public Object[] getData() {
		return new Object[] { id, userId, projectId };
	}

}
