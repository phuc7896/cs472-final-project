package edu.mum.cs.cs472.task_mgnt.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;

public class Project extends BaseModel<Long> {
	
	private final String TableName = "project";
	private final String[] ColumnNames = { 
	"id", "name", "description", "createdDate" };
	
	private long id;
	private String name;
	private String description;
	// private ProjectStatus status;
	private LocalDateTime createdDate;
	
	public Project() { }

	public Project(String name, String description) {
		super();
		this.name = name;
		this.description = description;
		this.createdDate = LocalDateTime.now();
		// this.status = ProjectStatus.New;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

//	public ProjectStatus getStatus() {
//		return status;
//	}
//
//	public void setStatus(ProjectStatus status) {
//		this.status = status;
//	}
	
	public LocalDateTime getCreatedDate() {
		return createdDate;
	}
	
	public void setCreatedDate(LocalDateTime createdDate) {
		this.createdDate = createdDate;
	}
	
	@Override
	public String toString() {
		
		return String.format("Project: {"
				+ "id: %s,"
				+ "name: %s,"
				+ "description: %s}", 
				id, name, description);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public void loadData(Object[] data) {
		id = new Long((int)data[0]);
		name = data[1].toString();
		description = data[2].toString();
		Timestamp sqlDate = (Timestamp)data[3]; 
		createdDate = sqlDate.toLocalDateTime();
	}
	
	@Override
	public Object[] getData() {
		return new Object[] { id, name, description, createdDate };
	}

}
