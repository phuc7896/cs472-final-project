package edu.mum.cs.cs472.task_mgnt.model;

import java.sql.Timestamp;
import java.time.LocalDateTime;


public class User extends BaseModel<Long> {
	
	public static final String RoleAdmin = "admin";
	public static final String RoleUser = "user";
	
	private final String TableName = "user";
	private final String[] ColumnNames = { 
	"id", "userName", "password", "firstName", "lastName", "email", "role", "dob" };
	
	private long id;
	private String username;
	private String password;
	private String firstName;
	private String lastName;
	private String email;
	private String role;
	private LocalDateTime dob;
	
	public User() {
		
	}
	
	public User(
			String username, 
			String password, 
			String firstName, 
			String lastName, 
			String email, 
			LocalDateTime dob) {
		super();
		this.username = username;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.email = email;
		this.role = RoleAdmin;
		this.dob = dob;
	}

	@Override
	public Long getId() {
		return id;
	}
	
	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public String getRole() {
		return role;
	}
	
	public void setRole(String role) {
		this.role = role;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public LocalDateTime getDob() {
		return dob;
	}
	
	public void setDob(LocalDateTime dob) {
		this.dob = dob;
	}
	
	@Override
	public String toString() {
		return String.format("User {"
				+ "userName: %s,"
				+ "firstName: %s,"
				+ "lastName: %s,"
				+ "..."
				+ "}", 
				username, firstName, lastName);
	}
	
	@Override
	public String getTableName() {
		return TableName;
	}
	
	@Override
	public String[] getColumnNames() {
		return ColumnNames;
	}
	
	@Override
	public void loadData(Object[] data) {
		id = (long) (int)data[0];
		username = data[1].toString();
		password = data[2].toString();
		firstName = data[3].toString();
		lastName = data[4].toString();
		email = data[5].toString();
		role = data[6].toString();
		// role = UserRole.valueOf(roleString);
		// dob = null; // TODO Convert java date
		Timestamp sqlDate = (Timestamp)data[7];
		LocalDateTime dt = sqlDate.toLocalDateTime();
		dob = dt;
	}
	
	@Override
	public Object[] getData() {
		return new Object[] {
			id, username, password, firstName, lastName, email, role, dob
		};
	}
	
}
