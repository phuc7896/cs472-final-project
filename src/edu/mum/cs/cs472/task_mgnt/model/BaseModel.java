package edu.mum.cs.cs472.task_mgnt.model;

public abstract class BaseModel<ID> {
	// protected ID id;
	
	public abstract ID getId();
	/* Support Long id only */
	public abstract void setId(Long id);
	
	
	public String getIdColumnName() {
		return "id";
	}
	
	public abstract String getTableName();
	public abstract String[] getColumnNames();
	public abstract void loadData(Object[] data);
	public abstract Object[] getData();
}
