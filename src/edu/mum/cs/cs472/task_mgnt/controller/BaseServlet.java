package edu.mum.cs.cs472.task_mgnt.controller;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.mum.cs.cs472.task_mgnt.util.Logger;

public abstract class BaseServlet extends HttpServlet {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4170304382675640356L;
	
	protected boolean traceRequest = true;
	
	protected Logger logger;
	
	public BaseServlet() {
		super();
		
		logger = Logger.getLogger(this.getClass().getCanonicalName());
		
		logger.debug("Initialization of controller ::: " + this.getClass().getName());
	}
	
	@Override
	protected void doGet(
		HttpServletRequest req, 
		HttpServletResponse resp) 
			throws ServletException, IOException {
		// super.doGet(req, resp);
	}
	
	@Override
	protected void doPost(
		HttpServletRequest req, 
		HttpServletResponse resp) 
			throws ServletException, IOException {
		// super.doPost(req, resp);
	}
	
	protected String getBody(HttpServletRequest request) throws IOException {

	    String body = null;
	    StringBuilder stringBuilder = new StringBuilder();
	    BufferedReader bufferedReader = null;

	    try {
	        InputStream inputStream = request.getInputStream();
	        if (inputStream != null) {
	            bufferedReader = new BufferedReader(new InputStreamReader(inputStream));
	            char[] charBuffer = new char[128];
	            int bytesRead = -1;
	            while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
	                stringBuilder.append(charBuffer, 0, bytesRead);
	            }
	        } else {
	            stringBuilder.append("");
	        }
	    } catch (IOException ex) {
	        throw ex;
	    } finally {
	        if (bufferedReader != null) {
	            try {
	                bufferedReader.close();
	            } catch (IOException ex) {
	                throw ex;
	            }
	        }
	    }

	    body = stringBuilder.toString();
	    
	    return body;
	}
	

}
