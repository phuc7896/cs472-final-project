package edu.mum.cs.cs472.task_mgnt.controller;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.mum.cs.cs472.task_mgnt.model.Project;
import edu.mum.cs.cs472.task_mgnt.model.Task;
import edu.mum.cs.cs472.task_mgnt.model.User;
import edu.mum.cs.cs472.task_mgnt.service.IProjectService;
import edu.mum.cs.cs472.task_mgnt.service.ITaskService;
import edu.mum.cs.cs472.task_mgnt.service.IUserService;
import edu.mum.cs.cs472.task_mgnt.service.impl.ServiceFactory;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/project")
public class ProjectServlet extends BaseServlet {
	
	private static final long serialVersionUID = 1L;
	
	private IProjectService projectService;
	private ITaskService taskService;
	private IUserService userService;
	   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectServlet() {
        super();
        projectService = ServiceFactory.getInstance().getProjectService();
        taskService = ServiceFactory.getInstance().getTaskService();
        userService = ServiceFactory.getInstance().getUserService();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {
		
		super.doGet(request, response);
		
		List<Project> projects = projectService.getAllProjects();
		request.setAttribute("projects", projects);

		if (projects.size() > 0) {
			long projectId = projects.get(0).getId();
			try {
				projectId = Long.parseLong(request.getParameter("projectId"));	
			} catch(Exception ex) {}
			
			Map<Long, List<User>> tasks = new HashMap<Long, List<User>>();
			List<Task> listTask = taskService.getTasksByProject(projectId);
			listTask.forEach(task -> {
				List<User> users = userService.getUsersByTaskId(task.getId());
				tasks.put(task.getId(), users);
			});

			request.setAttribute("tasks", listTask);
			request.setAttribute("projectId", projectId);
			request.setAttribute("taskUserMap", tasks);
		}
		
		String targetJspPath = "/jsp/page/project.jsp";

		request
        .getRequestDispatcher(targetJspPath)
        .forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {

		logger.debug("doPost --> ");
		
		
		doGet(request, response);
	}

}
