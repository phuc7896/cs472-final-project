package edu.mum.cs.cs472.task_mgnt.controller;

import java.io.IOException;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.mum.cs.cs472.task_mgnt.dao.TodoDAO;
import edu.mum.cs.cs472.task_mgnt.model.Todo;
import edu.mum.cs.cs472.task_mgnt.service.ITodoService;
import edu.mum.cs.cs472.task_mgnt.service.impl.TodoServiceImpl;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/todo")
public class TodoServlet extends BaseServlet {
	
	private static final long serialVersionUID = 1L;
	
	// private ContactDAO contactDAO;
	private TodoDAO todoDAO;
	private ITodoService todoService;
	   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public TodoServlet() {
        super();
        
        // contactDAO = new ContactDAO();
        todoDAO = new TodoDAO();
		todoService = new TodoServiceImpl();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response)
			throws ServletException, IOException {
		
		super.doGet(request, response);
		
		request.setAttribute("pageTitle", "Todo");
		// testDAO_contact();
		// testDAO_todo();
		
		try {
			// Test findAll
			List<Todo> todos = todoDAO.findAll();
			request.setAttribute("todos", todos);
			
			// List<Contact> contacts = contactDAO.findAll();			
			// request.setAttribute("contactMessages", contacts);
		} catch (Exception e) { 
			logger.error("An error occurs while doing todo test ...", e);
		}
		
		String targetJspPath = "/jsp/page/todo.jsp";

		request
        .getRequestDispatcher(targetJspPath)
        .forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {

		logger.debug("doPost --> ");
		
		Todo todo = readTodoFromRequest(request);
		Todo dbTodo = todoService.addTodo(todo);
		logger.info(dbTodo.toString());
		
		response.sendRedirect("./todo");
		// doGet(request, response);
	}
	
	private Todo readTodoFromRequest(HttpServletRequest request) {
		
		String title = request.getParameter("title");
		String description = request.getParameter("description");
		
		Todo todo = new Todo(title, description);
		
		return todo;
	}
}
