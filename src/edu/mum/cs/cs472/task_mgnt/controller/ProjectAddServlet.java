package edu.mum.cs.cs472.task_mgnt.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import edu.mum.cs.cs472.task_mgnt.model.Project;
import edu.mum.cs.cs472.task_mgnt.model.User;
import edu.mum.cs.cs472.task_mgnt.service.IProjectService;
import edu.mum.cs.cs472.task_mgnt.service.IUserService;
import edu.mum.cs.cs472.task_mgnt.service.impl.ServiceFactory;

/**
 * Servlet implementation class ProjectAddServlet
 */
@WebServlet("/project-add")
public class ProjectAddServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	
	private IProjectService projectService;
	private IUserService userService;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public ProjectAddServlet() {
        super();
        projectService = ServiceFactory.getInstance().getProjectService();
        userService = ServiceFactory.getInstance().getUserService();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.getWriter().append("Served at: ").append(request.getContextPath());
		String targetJspPath = "/jsp/page/project-add.jsp";
		
		List<User> users = userService.getAllUsers();
		request.setAttribute("users", users);
		
		request
        .getRequestDispatcher(targetJspPath)
        .forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String projectName = request.getParameter("name");
		String projectDescription = request.getParameter("desc");
		String[] projectUsers = request.getParameterValues("users");
		
		String error = "";
		if(projectName == null) {
			error = error + "Project name is required \n";
		}
		if(projectDescription == null) {
			error = error + "Description name is required \n";
		}
		
		List<Long> listUser = new ArrayList<Long>();
		for (int i = 0; i < projectUsers.length; i++) {
			listUser.add(Long.parseLong(projectUsers[i]));
		}
		Project checkProject = projectService.getProjectByName(projectName);
		if (checkProject != null) {
			error = error + "Project name is duplicated \n";
			request.setAttribute("errors", error);
			doGet(request, response);
		} else {
			Project newProject = projectService.addProject(new Project(projectName, projectDescription), listUser);
			
			response.sendRedirect("project?projectId="+newProject.getId());
		}
		
	}

}
