package edu.mum.cs.cs472.task_mgnt.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.mum.cs.cs472.task_mgnt.model.User;
import edu.mum.cs.cs472.task_mgnt.service.IUserService;
import edu.mum.cs.cs472.task_mgnt.service.impl.ServiceFactory;

/**
 * Servlet implementation class UserServlet
 */
@WebServlet("/login")
public class LoginServlet extends BaseServlet {
	
	private static final long serialVersionUID = 1L;
	
	// private IProjectService projectService;
	// private ITaskService taskService;
	private IUserService userService;
	   
    /**
     * @see HttpServlet#HttpServlet()
     */
    public LoginServlet() {
        super();
        // projectService = ServiceFactory.getInstance().getProjectService();
        // taskService = ServiceFactory.getInstance().getTaskService();
        userService = ServiceFactory.getInstance().getUserService();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {
		
		super.doGet(request, response);
		HttpSession session = request.getSession();
		session.invalidate();
		String targetJspPath = "/jsp/page/signin.jsp";
		request
        .getRequestDispatcher(targetJspPath)
        .forward(request, response);
	}
	
	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(
		HttpServletRequest request, 
		HttpServletResponse response) 
			throws ServletException, IOException {

		String username = request.getParameter("username");
		String password = request.getParameter("password");

		User loginUser = userService.login(username, password);
		if (loginUser == null) {
			request.setAttribute("error", "Username or password is not correct");
			doGet(request, response);
		} else {
			// create session object
			HttpSession session = request.getSession();
			
			//set session user by userId
			session.setAttribute("user", loginUser.getId());
			session.setMaxInactiveInterval(30*60);
			
			//
			response.sendRedirect("project");
		}
	}

}
