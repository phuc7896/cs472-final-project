
<%@ taglib uri="/WEB-INF/custom-tag/task-status.tld" prefix="ct"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
  pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="description" content="CS472-WAP" />
<meta name="keywords" content="HTML, CSS" />

<link
  href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cosmo/bootstrap.min.css"
  rel="stylesheet"
  integrity="sha384-uhut8PejFZO8994oEgm/ZfAv0mW1/b83nczZzSwElbeILxwkN491YQXsCFTE6+nx"
  crossorigin="anonymous">
<link rel="stylesheet" href="css/style.css">
<link rel="stylesheet" href="css/page-project.css">
<link rel="stylesheet" href="css/detail-task.css">
<link
  href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.min.css"
  rel="stylesheet">

<title>CS472-WAP ::: Task management!</title>
</head>
<body>
  <header>
    <%@ include file="../fragment/header.jsp"%>
  </header>

  <!------ Include the above in your HEAD tag ---------->
  <div class="container mt-4" style="margin-bottom: 3em;">
    <div class="row">
      <div class="col-lg-2"></div>
      <div class="col-md-offset-2 col-md-8 col-lg-offset-3 col-lg-8">
        <div class="well profile">
          <h2>Task Name: ${task.name}</h2>
          <input type="hidden" id="taskId" value="${task.id}"/>
          <input type="hidden" id="taskStatus" value="${task.status}"/>
          <p>
            <strong>Project:</strong> ${project.name}
          </p>
          <p>
            <strong>Created Date: </strong> ${task.createdDate}
          </p>
          <p>
            <strong>Description: </strong>${task.description}
          </p>
          <p>
            <strong>Who works on task:</strong>
            <c:forEach items="${users}" var="user">
              <span>${user.username}</span>
            </c:forEach>
          </p>
          <p style="font-weight: bold;">
            <ct:task-status task='${ task }'></ct:task-status>
          </p>
          
          <div class="col-xs-12 divider text-center">
            <div class="col-xs-12 emphasis">
              <button class="btn btn-success btn-block">
                <span class="fa fa-plus-circle"></span> Start Date:
                ${task.startDate }
              </button>
            </div>
            <div class="col-xs-12 emphasis">
              <button class="btn btn-info btn-block">
                <span class="fa fa-user"></span> End Date: ${task.endDate }
              </button>
            </div>
          </div>
          
          <div class="col-xs-12 divider">
            <h3>Change status</h3>
            <div class="form-group">
              <label for="selStatus">Select a status</label>
              <select class="form-control" id="selStatus" name="selStatus" required>
                <option value="1">New</option>
                <option value="2">In-Process</option>
                <option value="3">Finishd</option>
              </select>
            </div>
            <button id="btnUpdateTaskStatus" class="btn btn-outline-primary">Update</button>
          </div>
        </div>
      </div>
    </div>
  </div>

  <footer class="page-footer font-small blue bg-primary">
    <%@ include file="../fragment/footer.jsp"%>
  </footer>

  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
    crossorigin="anonymous"></script>
  <script
    src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
    crossorigin="anonymous"></script>
  <script
    src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
    crossorigin="anonymous"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  
  <script src="js/ajax.js"></script>
  <script src="js/page-task-detail.js"></script>
  <script src="js/app.js"></script>
</body>
</html>
