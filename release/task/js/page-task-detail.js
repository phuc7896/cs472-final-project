(function() {
  
  let logger = console;
//  let ajax = new Ajax();
  
  setTimeout(main, 1);
  
  function main() {
    let taskStatus = $('#taskStatus').val();
    $("#selStatus").val(taskStatus);
    $('#btnUpdateTaskStatus').bind('click', function() {
      let newStatus = $('#selStatus').val();
      let taskId = $('#taskId').val();
      let task = {
          id: parseInt(taskId),
          status: parseInt(newStatus)
      }
      let ajax = new Ajax();
      ajax.updateTaskStatus(task)
      .then((resp) => {
        logger.info('Update Task Status responded -> ', resp);
        $('.task-status').html(getTaskStatusString(task.status));
      })
      .catch((err) => {
        logger.error('Failed to udpate Task Status', err);
      })
      
    });
  }
  
  function getTaskStatusString(status) {
    let t = 'unknown';
    switch(status) {
    case 1:
      t = 'New';
      break;
    case 2:
      t = 'InProcess';
      break;
    case 3:
      t = 'Finished';
      break;
    }
    
    return t;
  }
  
})();