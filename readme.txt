MUM.CS.CS472-WAP - Final Project delivery

This document descibe process to reproduce Task Management Software.

[project-root]
-src
	-> Include all java implementation
-docs
	-> Include softawre documentation, demo database, ...
-release
--task
--task.war
	-> Include Tomcat deployable package. Either folder task or file task.war is find for deployment 
-screenshots
-readme.txt
	-> this document


1. Software requirement:

Software is developed and tested under below requirement:
1. Tomcat 9.0.24
2. Java 8
3. MySQL 8.0.17

II. MySQL database: 

Db Name: ProjectsManagement
user: cs472
password: cs472

Demo database: ./docs/projects_management.sql

Default test users:
| admin    | 1234     |
| test     | 1234     |
| dev      | 1234     |
| user     | 1234     |

