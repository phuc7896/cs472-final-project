<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="CS472-WAP" />
  <meta name="keywords" content="HTML, CSS" />

  <link 
    href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cosmo/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-uhut8PejFZO8994oEgm/ZfAv0mW1/b83nczZzSwElbeILxwkN491YQXsCFTE6+nx" 
    crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/page-project.css">
  <title>CS472-WAP ::: Task management!</title>
</head>
<body>
<header>
<%@ include file="../fragment/header.jsp" %>
</header>

<!-- Create new TODO -->
<section class="page-content container">
  <div class="row">
    <div class="col-md-6 col-sm-12">
      <form id="frm-create-todo" method="POST" action="./todo" class="needs-validation">
    
        <legend>Create New Todo!!!</legend>
    
        <!-- Title -->
        <div class="form-group">
          <label for="txtTitle">*Title</label> 
          <input type="text" id="txTitle" name="title"
            class="form-control" aria-describedby="titleIdHelp" 
            placeholder="Title" value="${ param.title }">
        </div>
        
        <!-- Description -->
        <div class="form-group">
          <label for="txtDescription">*Desciption</label>
          <textarea class="form-control" id="txtDescription"
            name="description" placeholder="Description">${ param.description }</textarea>
        </div>
        
        <button type="submit" id="btnSubmit" class="btn btn-primary">Submit</button>
      </form>  
    </div>
    
    <div class="col-md-6 col-sm-12">
      <!-- Nothing -->
    </div>
  </div>
  
</section>

<!-- List exist TODOs -->
<section class="page-content container">
  <h2>List of todos</h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Title</th>
        <th scope="col">Description</th>
        <th scope="col">Created Date</th>
        <th scope="col">Status</th>
        <th scope="col">&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach var="todo" items="${todos}"
        varStatus="iteration">
        <tr>
          <th scope="row"><c:out value="${iteration.index+1}"></c:out>.</th>
          <td><c:out value="${todo.title}"></c:out></td>
          <td><c:out value="${todo.description}"></c:out></td>
          <td><c:out value="${todo.createdAt}"></c:out></td>
          <td><c:out value="${todo.status}"></c:out></td>
          <td>&nbsp;</td>
        </tr>
      </c:forEach>
    </tbody>
  </table>
</section>
<hr>
<section class="page-content container">
  <h2>List of Contact Messages</h2>
  <table class="table table-striped">
    <thead>
      <tr>
        <th scope="col">#</th>
        <th scope="col">Customer Name</th>
        <th scope="col">Gender</th>
        <th scope="col">Category</th>
        <th scope="col">Message</th>
        <th scope="col">&nbsp;</th>
        <th scope="col">&nbsp;</th>
      </tr>
    </thead>
    <tbody>
      <c:forEach var="contactData" items="${contactMessages}"
        varStatus="iteration">
        <tr>
          <th scope="row"><c:out value="${iteration.index+1}"></c:out>.</th>
          <td><c:out value="${contactData.name}"></c:out></td>
          <td><c:out value="${contactData.gender}"></c:out></td>
          <td><c:out value="${contactData.category}"></c:out></td>
          <td><c:out value="${contactData.message}"></c:out></td>
          <td>&nbsp;</td>
          <td>&nbsp;</td>
        </tr>
      </c:forEach>
    </tbody>
  </table>
</section>

<footer class="page-footer font-small blue bg-primary">
<%@ include file="../fragment/footer.jsp" %>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="js/app.js"></script>
</body>
</html>
