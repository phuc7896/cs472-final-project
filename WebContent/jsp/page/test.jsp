<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="CS472-WAP" />
  <meta name="keywords" content="HTML, CSS" />

  <link 
    href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cosmo/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-uhut8PejFZO8994oEgm/ZfAv0mW1/b83nczZzSwElbeILxwkN491YQXsCFTE6+nx" 
    crossorigin="anonymous">
  <link rel="stylesheet" href="css/style.css">
  <link rel="stylesheet" href="css/page-test.css">
  <title>CS472-WAP ::: Task management!</title>
</head>
<body>
<header>
<%@ include file="../fragment/header.jsp" %>
</header>
<div class="container">
  <h2>Test page</h2>
  
  <section class="page-content">
    <h2>Users</h2>
    <div class="row">
      <div class="col-md-6">
        <h3 style="color: red;">Not works</h3>
        <form id="frm-register" class="needs-validation" novalidate>

        <!-- Student Id -->
        <div class="form-group">
          <label for="txtStudentId">Student ID</label>
          <input type="text" class="form-control" id="txtStudentId" required
            aria-describedby="studentIdHelp" placeholder="000-XX-XXXX"
            pattern="^(000-)[a-zA-Z0-9]{2}-[a-zA-Z0-9]{4}">
          <div class="invalid-feedback">Please enter a valid id.</div>
          <small id="studentIdHelp" class="form-text text-muted">Your unique student identification number.</small>
        </div>

        <!-- First name -->
        <div class="form-group">
          <label for="txtFirstName">First Name</label>
          <input type="text" class="form-control" id="txtFirstName" required placeholder="e.g. John">
          <div class="invalid-feedback">First name is required.</div>
        </div>

        <!-- Select -->
        <div class="form-group">
          <label for="selType">Select a Type</label>
          <select class="form-control" id="selType" name="selType" required>
            <option value="checking">Checking</option>
            <option value="saving">Saving</option>
            <option value="none">None</option>
          </select>
        </div>

        <button type="submit" id="btnSubmit" class="btn btn-primary">Submit</button>

      </form>
      </div>
      
      <div class="col-md-6">
      
        <table class="table">
          <tbody>
            <tr><th>DbID</th><th>UserName</th><th>First Name</th><th>LastName</th><th>EmailName</th></tr>
            <c:forEach items="${users}" var="user">
            <tr>
              <td>${ user.id }</td>
              <td>${ user.username }</td>
              <td>${ user.firstName }</td>
              <td>${ user.lastName }</td>
              <td>${ user.email }</td>
            </tr>
            </c:forEach>
            
          </tbody>
        </table>
      </div>
    </div>
  </section>
  
  <section class="page-content">
    <h2>Projects</h2>
    
    <div class="row">
      <div class="col-md-6">
        <h3>Form add Project</h3>
      </div>
      <div class="col-md-6">
      
      <table class="table">
        <tbody>
          <tr><th></th><th>Name</th><th>Description</th><th>Created Date</th></tr>
          <c:forEach items="${projects}" var="project">
          <tr>
            <td>${ project.id }</td>
            <td>${ project.name }</td>
            <td>${ project.description }</td>
            <td>${ project.createdDate }</td>
          </tr>
          </c:forEach>
          
        </tbody>
      </table>
      </div>
    </div>
  </section>
  
  <section class="page-content row">
    <h2>Task</h2>
    <div class="row">
      <div class="col-md-6">
        <h3>Form add Task</h3>
      </div>
      <div class="col-md-6">
        <table class="table">
          <tbody>
            <tr><th></th><th>Name</th><th>Description</th><th>Created Date</th></tr>
            <c:forEach items="${tasks}" var="task">
            <tr>
              <td>${ task.id }</td>
              <td>${ task.name } - ${ task.status }</td>
              <td>${ task.description }</td>
              <td>${ task.createdDate }</td>
            </tr>
            </c:forEach>
            
          </tbody>
        </table>
      </div>
    </div>
  </section>
</div>

<footer class="page-footer font-small blue bg-primary">
<%@ include file="../fragment/footer.jsp" %>
</footer>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</body>
</html>
