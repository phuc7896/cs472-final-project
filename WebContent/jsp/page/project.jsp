<%@ page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri = "http://java.sun.com/jsp/jstl/core" prefix = "c" %>
<%@ taglib uri = "/WEB-INF/custom-tag/task-status.tld" prefix="ct" %>
<%@ taglib uri = "/WEB-INF/custom-tag/task-users.tld" prefix="tu" %>
<%@ taglib uri = "/WEB-INF/custom-tag/task-start-end.tld" prefix="tse" %>

<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8" />
  <meta name="description" content="CS472-WAP" />
  <meta name="keywords" content="HTML, CSS" />

  <link 
    href="https://stackpath.bootstrapcdn.com/bootswatch/4.3.1/cosmo/bootstrap.min.css" 
    rel="stylesheet" 
    integrity="sha384-uhut8PejFZO8994oEgm/ZfAv0mW1/b83nczZzSwElbeILxwkN491YQXsCFTE6+nx" 
    crossorigin="anonymous">
  <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous">
 
   <link rel="stylesheet" href="css/style.css">
   <link rel="stylesheet" href="css/page-project.css">
  
  <title>CS472-WAP ::: Task management!</title>
</head>
<body>
<header>
<%@ include file="../fragment/header.jsp" %>
</header>

<div class="container page-content">

  <h1 class="my-4">All Projects</h1>
  <div class="row">

    <!-- List Project -->
    <div class="col-lg-3 col-md-4 col-sm-12">
        
      <div class="list-group">
        <button class="list-group-item btn btn-outline-primary"
          onclick="window.location.href='project-add'">
          <i class="fa fa-plus-circle" aria-hidden="true"></i>&nbsp; Add Project</button>
        
        <ul id="ls-project">
          <c:forEach items="${projects}" var="project">
            <li class='<c:if test="${ project.id == projectId }">active</c:if>'>
              <a class="list-group-item" 
                href="project?projectId=${project.id}">${project.name}</a>
            </li>
          </c:forEach>
        </ul>
        
      </div>
    </div>
    <!-- /.col-lg-3 -->

    <div class="col-lg-9 col-md-8 col-sm-12">
       
      <c:if test="${ projectId > 0}">
        <button class="list-group-item btn btn-outline-primary"
          onclick="window.location.href='task-add?projectId=${ projectId }'"> Add Task</button>
      </c:if>
      <div class="row" style="margin-top: 2px;">
        <c:forEach items="${tasks}" var="task">
          <div class="col-lg-4 col-md-6 mb-4">
              <div class="card h-100">
                <div class="card-body">
                  <h4 class="card-title">
                    <a href="taskdetail?id=${task.id}">${task.name}</a>
                  </h4>
                  <p class="card-text task-desc"><b>Task Description:</b> ${task.description }</p>
                  <p class="card-text">
                    <tu:task-users users="${ taskUserMap[task.id] }"/>
                  </p>
                </div>
                <div class="card-footer">
                  <ct:task-status task='${ task }'></ct:task-status>
                  <tse:task-start-end task="${ task }"/>
                </div>
              </div>
            </div>
        </c:forEach>
      </div>		

    </div>
  </div>
</div>

<footer class="page-footer font-small blue bg-primary">
<%@ include file="../fragment/footer.jsp" %>
</footer>

<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="js/bootstrap.bundle.min.js"></script>

<script src="js/ajax.js"></script>
<script src="js/page-project.js"></script>
<script src="js/app.js"></script>
</body>
</html>
