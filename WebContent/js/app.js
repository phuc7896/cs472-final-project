(function() {
  
  "use strict";
  
  let logger = console;
  
  setTimeout(main, 10);
  
  function main() {
    let currentRoute = $('#currentRoute').val();
    logger.info('Current route -> ' + currentRoute);
    
    $('.nav-item').removeClass('item-active');
    if(currentRoute === 'index.jsp') {
      $($('.nav-item')[0]).addClass('item-active');
    } else {
      $('.item-' + currentRoute).addClass('item-active');  
    }
    
  }
  
})();